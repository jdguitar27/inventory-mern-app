const express = require('express');
const router = express.Router();
const Product = require('../models/product');

router.get('/', async (req, res) => {
    try 
    {
        const products = await Product.find().populate('category', '_id name').sort({createdAt: 'desc'});
        res.json(products);
    } catch (err) {
        console.log(err);
    }
});

router.post('/', async (req, res) => {
    try 
    {
        const product = await Product.create(req.body);
        res.json({
            message: 'Product succefuly created',
            product
        })
    } catch (err) {
        console.log(err);
        res.statusCode = 500;
        res.send({message: 'Error ocurred trying to insert the product', error: err.message});
    }
});

router.put('/', (req, res) => {
    
});

router.delete('/', (req, res) => {
    
});

module.exports = router;