const express = require('express');
const router = express.Router();
const Category = require('../models/category');

router.get('/', async (req, res) => {
    try
    {
        var categories = await Category.find().populate('subCategories', '_id, name').sort({name: 'asc'});
        categories = categories.filter(categ => categ.subCategories.length > 0)
        res.json(categories);
    } 
    catch (err) 
    {
        console.log(err);
        res.statusCode = 500;
        res.send({message: 'Error ocurred trying to read the categories', error: err.message});
    }
});

router.post('/', async (req, res) => {
    try
    {
        const category = await Category.create(req.body);
        if(category.categoryParent !== undefined)
        {
            const parent = await Category.findById(category.categoryParent);
            parent.subCategories = [...parent.subCategories, category._id];
            parent.save();
        }
        res.json({
            message: "Category created successfully!",
            category
        });
    }
    catch (err) 
    {
        console.log(err);
        res.statusCode = 500;
        res.send({message: 'Error ocurred trying to insert the category', error: err.message});
    }
});

module.exports = router;