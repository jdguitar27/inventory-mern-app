import React from 'react';
import Navbar from './Navbar';

class Header extends React.Component {
    render () {
        return (
            <Navbar data={{nombre: "José", apellido: "Durán"}}/>
        );
    }
}

export default Header;