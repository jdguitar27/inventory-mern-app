import React, { Component } from 'react'
import { Tooltip } from 'reactstrap';
export default class CustomTooltip extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tooltipOpen: false
        };
        
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            tooltipOpen: !this.state.tooltipOpen
        });
    }

    render() {
        return (
            <React.Fragment>
                { this.props.children }
                <Tooltip 
                    placement={ this.props.tooltipPlacement } 
                    isOpen={ this.state.tooltipOpen } 
                    target={ this.props.tooltipTarget} 
                    toggle={ this.toggle }
                >
                    { this.props.tooltipText }
                </Tooltip>
            </React.Fragment>
        );
    }
}
