import React from 'react';
import { Collapse, ListGroup, ListGroupItem } from 'reactstrap';

class SubcategoryCollapse extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            collapse: false
        }

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState(state => ({ collapse: !state.collapse }));
    }


    render() {
        return(
            <Collapse isOpen={this.state.collapse}>
            </Collapse>
        );
    }
}