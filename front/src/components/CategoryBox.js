import React from 'react';
import { ListGroup, ListGroupItem, Collapse, Card, CardBody } from 'reactstrap';

class CategoryBox extends React.Component {

    constructor (props) {
        super(props);

        this.state = {
            categories: [],
            collapse: false
        }

        this.toggle = this.toggle.bind(this);
    }

    componentDidMount() {
        fetch('http://localhost:5000/categories')
        .then(res => res.json())
        .then(data => {
            this.setState({
                categories: data
            });
        });
    }

    render () {
        return(
            <ListGroup className="vh-90 overflow-auto">
                {this.state.categories.map((category) => {
                    return(
                        <ListGroupItem className="text-left" tag="button" onClick={this.toggle} key={category._id}>
                            {category.name.toUpperCase()}
                            {/* <span className="badge badge-primary badge-pill">{category.count}</span> */}
                            <Collapse isOpen={this.state.collapse}>
                                <Card>
                                    <CardBody>
                                    Anim pariatur cliche reprehenderit,
                                    enim eiusmod high life accusamus terry richardson ad squid. Nihil
                                    anim keffiyeh helvetica, craft beer labore wes anderson cred
                                    nesciunt sapiente ea proident.
                                    </CardBody>
                                </Card>
                            </Collapse> 
                        </ListGroupItem>
                    );
                })}
            </ListGroup>
        );
    }
}

export default CategoryBox;