import React from 'react';
import Item from './Item';
import { ListGroup } from 'reactstrap';

class ItemList extends React.Component 
{
    constructor (props) {
        super(props);

        this.state = {
            products: []
        }
    }

    componentDidMount() {
        
        fetch("http://localhost:5000/products")
        .then((res) => res.json())
        .then(data => {
            this.setState({
                products: data
            })
        })
        .catch(error => console.log(error));
    }

    render () 
    {
        const products = this.state.products.map((product) => <Item key={product._id} product={product} />);
        return (
            <ListGroup id="items-list" className="vh-90 overflow-auto">
                {products}                
            </ListGroup>
        );
    }
}

export default ItemList;