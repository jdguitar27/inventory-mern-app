import React from 'react';
import CustomTooltip from './CustomTooltip';
import {
    MdAccountCircle,
    MdViewList,
    MdNotifications,
    MdMail,
    MdSettings
} from 'react-icons/md';

class SideIconBar extends React.Component {

    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        this.props.userClick(event.currentTarget.id);
    }

    render() {
        return (
            <div className="d-inline-block" style={{ width: 50, zIndex: 10 }}>
                <div className="bg-gradient vh-100 d-inline-block fixed-top" style={{ width: 50, zIndex: 10 }}>
                    <ul className="list-unstyled text-center text-light py-3">
                        <li>
                            <CustomTooltip
                                tooltipText="Perfil de Usuario"
                                tooltipPlacement="right"
                                tooltipTarget="user"
                            >
                                <MdAccountCircle id="user" size="24" onClick={this.handleClick} className="my-2 icon-button" />
                            </CustomTooltip>
                        </li>
                        <li>
                            <CustomTooltip
                                tooltipText="Inventario"
                                tooltipPlacement="right"
                                tooltipTarget="inventory"
                            > 
                                <MdViewList id="inventory" size="24" onClick={this.handleClick} className="my-2 icon-button" />
                            </CustomTooltip>
                        </li>
                        <li>
                            <CustomTooltip
                                tooltipText="Notificaciones"
                                tooltipPlacement="right"
                                tooltipTarget="notifications"
                            > 
                                <MdNotifications id="notifications" size="24" className="my-2 icon-button" />
                            </CustomTooltip>
                        </li>
                        <li> 
                            <CustomTooltip
                                tooltipText="Mensajes"
                                tooltipPlacement="right"
                                tooltipTarget="messagges"
                            >
                                <MdMail id="messagges" size="24" className="my-2 icon-button" /> 
                            </CustomTooltip>  
                        </li>
                        <li>
                            <CustomTooltip
                                tooltipText="Configuraciones"
                                tooltipPlacement="right"
                                tooltipTarget="settings"
                            > 
                                <MdSettings id="settings" size="24" onClick={this.handleClick} className="my-2 icon-button" /> 
                            </CustomTooltip>
                        </li>
                    </ul>
                </div>
            </div>

        );
    }
}

export default SideIconBar;