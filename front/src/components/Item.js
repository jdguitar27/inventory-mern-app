import React from 'react';
import { Badge, ListGroupItem, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';

class Item extends React.Component {

    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        alert(this.props.product.name);
    }

    render() {
        return (
            <ListGroupItem className="cursor-pointer list-group-item-action flex-column align-items-start" onClick={this.handleClick}>
                <ListGroupItemHeading className="d-flex justify-content-between">
                    {this.props.product.name}
                    <small>
                        <Badge pill color="danger">{this.props.product.category.name.toUpperCase()}</Badge>
                    </small>
                </ListGroupItemHeading>
                <ListGroupItemText className="mb-1">{this.props.product.description}</ListGroupItemText>
                <small>Donec id elit non mi porta.</small>
            </ListGroupItem>
        );
    }
}

export default Item;