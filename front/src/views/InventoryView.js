import React from 'react';
import CategoryBox from '../components/CategoryBox';
import ItemList from '../components/ItemList';
import { Row, Col } from 'reactstrap';
import { MdDashboard } from 'react-icons/md';

class InventoryView extends React.Component {
	render() {
		return (
			<Row className="row pt-4 px-4" >
				<Col sm="12" md="3" id="sidenav" className="text-center">
					<h3>Categorías</h3>
					<CategoryBox />
				</Col>
				<Col sm="12" md="9" id="princ-content" className="my-sm-4 my-md-0">
					<Row className="align-content-between">
						<Col className="col"><h3>Contenido principal</h3></Col>
						<MdDashboard size="24" className="text-primary cursor-pointer mr-3 icon-button" />
					</Row>
					<ItemList />
				</Col>
			</Row>
		);
	}
}

export default InventoryView;