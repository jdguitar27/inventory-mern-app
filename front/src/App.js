import React from 'react';
// import Header from './components/Header';
import SideIconBar from './components/SideIconBar';
import InventoryView from './views/InventoryView';
import UserProfileView from './views/UserProfileView';
import SettingsView from './views/SettingsView';
import { Container } from 'reactstrap';

class App extends React.Component
{
	state = {
		view: "inventory"
	}

	handleUserIconClick = (viewName) => {
		this.setState ({
			view: viewName
		});
	}

	renderView() {
		switch(this.state.view)
		{
			case "inventory":
				return(<InventoryView />);
			case "user":
				return(<UserProfileView />);
			case "settings":
				return(<SettingsView />);
			default:
				return(<h1>Default View</h1>);
		}
	}
	
	render (){
		return(
			<React.Fragment>
				<SideIconBar userClick={this.handleUserIconClick}/>
				<Container fluid className="d-inline-block principal-view">
					{this.renderView()}					
				</Container>
			</React.Fragment>
		);
	}
}

export default App;
