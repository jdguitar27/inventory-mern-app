const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    name:        { type: String, required: [true, 'The field name is required'], unique: true },
    description: { type: String, required: [true, 'The field description is required'] },
    category:    { type: Schema.Types.ObjectId, ref: 'Category' },
    createdAt:   { type: Date, default: Date.now }
});

module.exports = mongoose.model('Product', ProductSchema);