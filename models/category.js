const monggose = require('mongoose');
const Schema = monggose.Schema;

const CategorySchema = new Schema({
    name:        { type: String, required: true, unique: true },
    description: { type: String },
    categoryParent: { type: Schema.Types.ObjectId, ref: 'Category' },
    subCategories: {type: [Schema.Types.ObjectId], ref: 'Category'},
    createdAt:   { type: Date, default: Date.now }
});

module.exports = monggose.model('Category', CategorySchema);