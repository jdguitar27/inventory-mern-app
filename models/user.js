const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email:     { type: String, required: [true, 'The field email is required'] },
    firstName: { type: String, required: [true, 'The field firstName is required'] },
    lastName:  { type: String, required: [true, 'The field lastName is required'] },
    password:  { type: String, required: [true, 'The field password is required'] },
    createdAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('User', UserSchema);