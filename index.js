const express = require('express');
const bodyParser = require('body-parser');
const productsRouter = require('./routes/products.routes');
const usersRouter = require('./routes/users.routes');
const categoriesRouter = require('./routes/categories.routes');
require('dotenv').config();

const app = express();

const port = process.env.PORT || 5000;

require('./db');

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.json());

app.use('/products', productsRouter);
app.use('/users', usersRouter);
app.use('/categories', categoriesRouter);

app.listen(port, () => {
    console.log(`Server running on port ${port}`)
});