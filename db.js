const mongoose = require('mongoose');

//connect to the database
mongoose.connect(process.env.DB, { useNewUrlParser: true, useCreateIndex: true })
  .then(() => console.log(`Database connected successfully`))
  .catch(err => console.log(err));